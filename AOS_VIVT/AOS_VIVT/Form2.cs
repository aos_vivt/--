﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace AOS_VIVT
{
    public partial class Form2 : Form
    {
        private Form1 frm;

        public Form2()
        {
            InitializeComponent();
        }
        
        public Form2(Form1 f)
        {
            InitializeComponent();
            frm = f;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frm.sPUBLIC_URI_CATALOG = textBox1.Text.ToString();
            label1.Text = "Ссылка сохранена";
            
            using (StreamWriter sw = new StreamWriter("sPUBLIC_URI_CATALOG.txt"))
            {
                sw.Write(textBox1.Text.ToString());
                sw.Close();
            }

        }
    }
}
