﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Threading;
using System.Diagnostics;



namespace AOS_VIVT
{
    public partial class Form1 : Form
    {
        //Строка с публичной ссылкой на каталог
        public string sPUBLIC_URI_CATALOG = null;
        //Строка с запросом на получение информации о публичном каталоге (яндекс диск)
        string sURI_REQUEST_INFO = "https://cloud-api.yandex.net/v1/disk/public/resources?public_key=";
        //Строка с запросом на получение прямой ссылки на скачивание публичного файла (яндекс диск)
        string sURI_REQUEST_DOWNLOAD = "https://cloud-api.yandex.net/v1/disk/public/resources/download?public_key=";
        //Строка с публичной ссылкой на скачивание фвйла
        string sPUBLIC_URI_ADDRESS = null;
        //Строка, содержащая имя текущего выбранного урока
        string sFILE_NAME = null;

        //Объект класса, хранящий всю информацию об ответе на запрос URI_REQUEST_INFO
        RootObject info_response;
        //Объект класса, хранящий всю информацию об ответе на запрос URI_REQUEST_DOWNLOAD
        Download_Response download_response;


        public Form1()
        {
            InitializeComponent();
            using (StreamReader sr = new StreamReader("sPUBLIC_URI_CATALOG.txt"))
            {
                sPUBLIC_URI_CATALOG = sr.ReadToEnd();
                sr.Close();
            }
        }


        //Кнопка "Обновить список уроков"
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                sPUBLIC_URI_ADDRESS = null;
                treeView1.Nodes.Clear();
                TreeNode lessonsNode = new TreeNode("Программирование");
                if (download_file("ListofLessons.json", sURI_REQUEST_INFO + sPUBLIC_URI_CATALOG))
                {
                    info_response = Serializer.Deserialize<RootObject>("ListofLessons.json");
                    for (int i = 0; i < info_response._embedded.total; i++)
                        lessonsNode.Nodes.Add(new TreeNode(info_response._embedded.items[i].name));
                }
                treeView1.Nodes.Add(lessonsNode);
                lessonsNode.Expand();
            }
            catch
            {

            }
        }
        

        //Метод для загрузки файлов с облачного хранилища
        private bool download_file(string filename, string uri)
        {
            WebClient WB = new WebClient();
            try
            {
                WB.DownloadFile(new Uri(uri), filename);
            }
            catch
            {
                MessageBox.Show("Нет соединения");
                return false;
            }
            return true;
        }

        //Кнопка "Запустить урок"
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (sPUBLIC_URI_ADDRESS == null)
                {
                    MessageBox.Show("Выберите урок из списка слева");
                    return;
                }
                download_file("Lessons.json", sURI_REQUEST_DOWNLOAD + sPUBLIC_URI_ADDRESS);
                download_response = Serializer.Deserialize<Download_Response>("Lessons.json");
                download_file(sFILE_NAME, download_response.href);
                Process.Start(sFILE_NAME);
            }
            catch
            {

            }
        }

        //Управление узлами treeView1
        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            try
            {
                textBox1.Text =
                                "Имя файла : " + info_response._embedded.items[e.Node.Index].name + Environment.NewLine +
                                "Создан : " + info_response._embedded.items[e.Node.Index].created + Environment.NewLine +
                                "Изменен : " + info_response._embedded.items[e.Node.Index].modified + Environment.NewLine +
                                "Директория : " + info_response._embedded.items[e.Node.Index].path + Environment.NewLine +
                                "Тип : " + info_response._embedded.items[e.Node.Index].type + Environment.NewLine +
                                "Публичный адресс: " + info_response._embedded.items[e.Node.Index].public_url + Environment.NewLine +
                                "Размер : " + info_response._embedded.items[e.Node.Index].size + Environment.NewLine;

                sPUBLIC_URI_ADDRESS = info_response._embedded.items[e.Node.Index].public_url; //Записать public_url выбранного узла
                sFILE_NAME = info_response._embedded.items[e.Node.Index].name; //Записать name выбранного узла
            }
            catch
            {
                
            }
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("AOS VIVT \nВерсия: 0.8");
        }

        private void настройкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 frm2 = frm2 = new Form2(this);
            frm2.Show();
        }
    }
}