﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOS_VIVT
{
    class Download_Response
    {
        public string href { get; set; }
        public string method { get; set; }
        public bool templated { get; set; }
    }
}
