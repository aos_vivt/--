﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOS_VIVT
{
    class RootObject
    {
        public string public_key { get; set; }
        public Embedded _embedded { get; set; }
        public string name { get; set; }
        public string created { get; set; }
        public string public_url { get; set; }
        public string modified { get; set; }
        public string path { get; set; }
        public string type { get; set; }
    }
}
