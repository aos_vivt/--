﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOS_VIVT
{
    class Item
    {
        public string public_key { get; set; }
        public string name { get; set; }
        public string created { get; set; }
        public string modified { get; set; }
        public string path { get; set; }
        public string type { get; set; }
        public string public_url { get; set; }
        public string media_type { get; set; }
        public string md5 { get; set; }
        public string mime_type { get; set; }
        public int size { get; set; }
        public string preview { get; set; }
    }
}
